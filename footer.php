<section class="group">
    <div class="col span_2_of_3">
        &nbsp; <br/>
        <?php wp_pagenavi(); ?>
        &nbsp; <br/>
    </div>
    <div class="col span_1_of_3">
    </div>
</section>

<div id="footer">
	&copy; <?php echo date("Y"); ?> Gyurka Jansen
</div>

</div>

<?php wp_footer(); ?>

</body>
</html>
