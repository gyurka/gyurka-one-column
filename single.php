<?php get_header(); ?>

<section class="group">
    <div class="col span_3_of_3">
    <!-- header -->
    </div>
</section>

<section class="group">
    <div class="col span_2_of_3">
        <!-- main -->

			<?php 
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post(); ?>
						<article>
						    <figure>
    							<a href="<?php the_permalink(); ?>">
    								<?php
    								if (get_post_type()=='dossier') { ?>
    									<div class="postLabel dossier">Dossier</div>
    								<?php }
    								the_post_thumbnail( 'single-thumb' ); 
    								?>
    							</a>
							</figure>
							<div class="singleContent">
							<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
							<?php
							$content_width = 620;
							the_content();
							?>
							</div>
							    
						</article>
						<?php
					} 
				} 
			?>
			
    </div>
    <div class="col span_1_of_3">
    <!-- sidebar -->
    </div>
</section>

<?php get_footer(); ?>