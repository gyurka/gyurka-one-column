<?php

add_theme_support( 'post-thumbnails', array( 'post', 'dossier' ) ); 
add_image_size( 'main-thumb', 600, 200, true); //hoofdthumbnail
add_image_size( 'single-thumb', 600, 300, true); //singlethumbnail

register_nav_menu('hoofdmenu', 'Hoofdmenu' );

if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name'=> 'Sidebar',
		'id' => 'sidebar-1',
		'description' => 'De enige sidebar',
		'before_widget' => '<section class="sidebaritem">',
		'after_widget' => '</section>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
));
}

/* 
 * Dossiers 
 * 
 */


/* Dossier PostType */

function create_dossier_type() {
		register_post_type( 'dossier',
			array(
				'labels' => array(
					'name' => __( 'Dossiers' ),
					'singular_name' => __( 'Dossier' )
				),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'dossier')
			)
		);
		add_post_type_support('dossier',
			array('author', 'revisions', 'title', 'editor', 'excerpt', 'comments', 'thumbnail'));	
			
}

add_action( 'init', 'create_dossier_type' );

/* Dossiers Taxonomy */

function dossiers_init() {
	// create a new taxonomy
	register_taxonomy(
		'dossiers',
		array('dossier'),
		array(
			'label' => __( 'Dossiers' ),
			'sort' => true,
			'args' => array( 'orderby' => 'term_order' ),
			'rewrite' => array( 'slug' => 'dossiers' ),
			'hierarchical'=>true	
		)
	);
}
add_action( 'init', 'dossiers_init' );
	
//register other taxomonies with posttype
function reg_cat() {
         register_taxonomy_for_object_type('category','dossier');
		 register_taxonomy_for_object_type('post_tag','dossier');
}

add_action('init', 'reg_cat');

//hooks

// Show posts of 'post' and 'dossier' post types on home page
add_action( 'pre_get_posts', 'add_my_post_types_to_query' );

function add_my_post_types_to_query( $query ) {
	if ( is_home() && $query->is_main_query() )
		$query->set( 'post_type', array( 'post', 'dossier' ) );
	return $query;
}

?>