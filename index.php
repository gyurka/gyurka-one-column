<?php get_header(); ?>

<section class="group">
    <div class="col span_3_of_3">
    <!-- header -->
    </div>
</section>

<section class="group">
    <div class="col span_2_of_3">
        <!-- main -->
             <?php 
                if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post(); ?>
                        <article>
                            <figure>
                                <a href="<?php the_permalink(); ?>">
                                    <?php
                                    if (get_post_type()=='dossier') { ?>
                                        <div class="postLabel dossier">Dossier</div>
                                    <?php }
                                    the_post_thumbnail( 'main-thumb' ); 
                                    ?>
                                </a>
                            </figure>
                            <div class="singleContent">
                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <?php
                            $oldcontent = get_the_content('');
                            $newcontent = strip_tags($oldcontent, '');
                            echo $newcontent
                            ?>
                            </div>
                            <footer>
                                <span class="more"><a href="<?php the_permalink(); ?>">&#10151;</a></span><span class="comments"><a href="<?php comments_link(); ?>"><?php comments_number( 'geen reacties', 'één reactie', '% reacties' ); ?></a></span>
                            </footer>
                        </article>
                        <?php
                    } 
                } 
            ?>
    </div>
    <div class="col span_1_of_3">
    <!-- sidebar -->
    </div>
</section>


<?php get_footer(); ?>
