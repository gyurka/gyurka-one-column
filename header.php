<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php wp_title(); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        
        <link href="https://fonts.googleapis.com/css?family=Merriweather:900" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="apple-touch-icon" sizes="57x57" href="/ico/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/ico/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/ico/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/ico/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/ico/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/ico/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/ico/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/ico/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/ico/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/ico/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/ico/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/ico/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/ico/favicon-16x16.png">
        <link rel="manifest" href="/ico/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ico/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <?php wp_head(); ?>
        

	    <!--[if lt IE 7]>
	    <style media="screen" type="text/css">
	    .col1 {
		    width:100%;
		}
	    </style>
	    <![endif]-->
    </head>
<body <?php body_class(); ?>>
<div id="fixed">

<div id="header">
    <figure>
        <a href="<?php echo site_url(); ?>">
            <img srcset="<?php bloginfo( 'template_url' ); ?>/gfx/gy_logo2x.jpg 2x" src="<?php bloginfo( 'template_url' ); ?>/gfx/gy_logo.jpg" alt="" class="" />
        </a>
    </figure>
</div>
<nav>
    <?php wp_nav_menu( array( 'theme_location' => 'hoofdmenu') ); ?>
</nav>